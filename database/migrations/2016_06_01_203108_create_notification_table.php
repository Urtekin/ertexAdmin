<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
     Schema::create('anouncements',function(Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->string('content',1024);
            //$table->string('pictures',1024);
            $table->boolean('isActive');
            $table->timestamps();
        });
        */
        Schema::create('notifications',function(Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->string('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications');
    }
}
