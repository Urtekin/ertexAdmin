<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;

use App\Anouncement;
use App\Pictures;

use Illuminate\Support\Facades\Input;
//use App\Http\Controllers\Input;
use DB;
use Storage;

class MobileControllerV1 extends Controller
{
  /*		$cities   = City::all();
		$counties = County::All();
	    return view('gen_mems.add', [

	        	'Cities' => $cities,
	        	// 'Counties' => $counties

	    ]);*/
  public function getAnouncementJson($id)
  {
    //$anouncement = Anouncement::findOrFail($id)->with('Pictures');
    $anouncement = Anouncement::with('Pictures')->orderBy('id','desc')->findOrFail($id);
    return $anouncement;
  }
  public function getAnouncementsJson()
  {
    $anouncements = Anouncement::with('Pictures')->orderBy('id','desc')->get();//anouncementları picture linkleri ile birlikte çektik.

    return $anouncements;
  }
  public function getLastAnouncementJson()
  {
    $anouncement = Anouncement::with('Pictures')->orderBy('id','desc')->first();
    return $anouncement;
  }
  public function getLuxuryPics()
  {
    $files  = array();

    foreach (glob("pictures/luxury/*.jpg") as $filename) {
        $files[] = $filename;
        //echo "$filename size " . filesize($filename) . "\n";
    }
    return($files);
  }
  public function getBusinessPics(){
    $files  = array();

    foreach (glob("pictures/business/*.jpg") as $filename) {
        $files[] = $filename;
        //echo "$filename size " . filesize($filename) . "\n";
    }
    return($files);
  }
}
