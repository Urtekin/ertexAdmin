<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;

use App\Anouncement;
use App\Pictures;

use Illuminate\Support\Facades\Input;
//use App\Http\Controllers\Input;
use DB;
use Storage;

class AnouncementsController extends Controller
{
    //
    public function sendNotification($title,$content)
    {
      $headings = array(
        "en" => $title
        );

      $content = array(
          "en" => $content
          );

        $fields = array(
          'app_id' => "a55f53bf-a380-498a-956c-9c1a1869f679",//OneSignal App ID
          'included_segments' => array('All'),
          'data' => array("type" => "haber"),
          'contents' => $content,
          'headings' => $headings
        );

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                               'Authorization: Basic ODBlNWU2MjUtMmRhZi00MzE1LWJkYzYtZDNmNGIxMWI2MTNh'));//OneSignal "Rest API Key"
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

    }
    public function __construct()
    {
        $this->middleware('auth');
        //$this->info("constructor calıştı.");
    }

	public function getPicDelete(Request $request, $id){

		$picture = Pictures::findOrFail($id);
		//return('/uploads/' . $picture->name);
		$picId = $picture->id;
		Storage::delete('/uploads/' . $picture->name);
		$picture->delete();
		return $picId;

	}
    public function getIndex(Request $request)
	{
	    return view('anouncements.index', []);
	}

	public function getAdd(Request $request)
	{
	    return view('anouncements.add', [
	        []
	    ]);
	}

	public function getUpdate(Request $request, $id)
	{

		$anouncement = Anouncement::findOrFail($id);
		$pics = $anouncement->pictures;

	    return view('anouncements.add', [
	        'model' => $anouncement	   ,'picModel' => $pics]);
	}

	public function getShow(Request $request, $id)
	{

		$anouncement = Anouncement::findOrFail($id);
		$pics = $anouncement->pictures;
	    return view('anouncements.show', [
	        'model' => $anouncement,
	        'picModel' => $pics
	        ]);
	}

	public function getGrid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM anouncements a ";
		if($_GET['search']['value']) {
			$presql .= " WHERE title LIKE '%".$_GET['search']['value']."%' ";
		}

		$presql .= "  order by id desc  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function postSave(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$anouncement = null;
		if($request->id > 0) {
			$anouncement = Anouncement::findOrFail($request->id);
		}
		else {
			$anouncement = new Anouncement;
			//dd('buraya girmiş');
			}
	    	$anouncement->title = $request->title;
	    	$anouncement->content = $request->content;
	    	//$anouncement->pictures = $request->pictures;
        if($request->isActive == "on")
	    	$anouncement->isActive = "1";
        else
        $anouncement->isActive = "0";

	    	    //$anouncement->user_id = $request->user()->id;
	   		$anouncement->save();

			$pictureNumber = 1;
				//dd($request->file('file'));
				foreach($request->file('file') as $onlyPicture)
				{
          if(!is_null($onlyPicture))
          {
  					$file = $onlyPicture;
  					  $rules = array(
        					'image' => 'mimes:jpeg,jpg,png,gif|required|max:100000' // max 10000kb
      					);

  				  	$validator = Validator::make([$file] , [$rules] );
  				  	if($validator->fails()){

  				  		return $this->errors(['message' => 'Dosyayı kaydederken hata oluştu', 'code' => 400]);
  				  	}
  				  	else
  				  	{
  				  	$destination_path = 'uploads/';
  				  	//return $_FILES;
  				  	//dd($file);
  				  	$fileName = str_random(6). '_'. $file->getClientOriginalName();
  				  	$file->move($destination_path,$fileName);

  						$picture = new Pictures;
  						$picture->anouncement_id = $anouncement->id;
  						$picture->name = $fileName;

  						$picture->save();
              }
					}

				}
          //dd($request);
          if($request->isActive == "on")
          {
            $this->sendNotification($request->title,$request->content);
          }


	    		return redirect('/anouncements/index');
	    	}



	public function getDelete(Request $request, $id) {

		$anouncement = Anouncement::findOrFail($id);
		$pictures = $anouncement->pictures;
		foreach ($pictures as $picture) {

			$picId = $picture->id;
			Storage::delete('/uploads/' . $picture->name);
			$picture->delete();
		}
		$anouncement->delete();
		return redirect('/anouncements/index');

	}



}
