<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Notification;

use DB;

class NotificationsController extends Controller
{
	public function sendNotification($title,$content)
	{
		$headings = array(
			"en" => $title
			);

		$content = array(
	      "en" => $content
	      );

	    $fields = array(
	      'app_id' => "a55f53bf-a380-498a-956c-9c1a1869f679",//OneSignal App ID
	      'included_segments' => array('All'),
	      'data' => array("type" => "genel"),
	      'contents' => $content,
	      'headings' => $headings
	    );

	    $fields = json_encode($fields);
	    print("\nJSON sent:\n");
	    print($fields);

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
	                           'Authorization: Basic ODBlNWU2MjUtMmRhZi00MzE1LWJkYzYtZDNmNGIxMWI2MTNh'));//OneSignal "Rest API Key"
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    curl_setopt($ch, CURLOPT_HEADER, FALSE);
	    curl_setopt($ch, CURLOPT_POST, TRUE);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

	    $response = curl_exec($ch);
	    curl_close($ch);

	}
    //
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getIndex(Request $request)
	{
	    return view('notifications.index', []);
	}

	public function getAdd(Request $request)
	{
	    return view('notifications.add', [
	        []
	    ]);
	}

	public function getUpdate(Request $request, $id)
	{
		$notification = Notification::findOrFail($id);
	    return view('notifications.add', [
	        'model' => $notification	    ]);
	}

	public function getShow(Request $request, $id)
	{
		$notification = Notification::findOrFail($id);
	    return view('notifications.show', [
	        'model' => $notification	    ]);
	}

	public function getGrid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT id,title,content,1,2 ";
		$presql = " FROM notifications a ";
		if($_GET['search']['value']) {
			$presql .= " WHERE title LIKE '%".$_GET['search']['value']."%' ";
		}

		$presql .= " order by id desc ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function postSave(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$notification = null;
		if($request->id > 0) { $notification = Notification::findOrFail($request->id); }
		else {
			$notification = new Notification;
		}

				$this->sendNotification($request->title,$request->content);
	    	    $notification->title = $request->title;
	    	    $notification->content = $request->content;
	    	    //$notification->user_id = $request->user()->id;
	    $notification->save();

	    return redirect('/notifications/index');

	}

	public function getDelete(Request $request, $id) {

		$notification = Notification::findOrFail($id);

		$notification->delete();
		return redirect('/notifications/index');

	}


}
