<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
    Route::get('/','HomeController@index');


    Route::get('/getAnouncementsJson' , 'MobileControllerV1@getAnouncementsJson');
    Route::get('/getAnouncementJson/{id}', 'MobileControllerV1@getAnouncementJson');
    Route::get('/getLuxuryPics','MobileControllerV1@getLuxuryPics');
    Route::get('/getBusinessPics','MobileControllerV1@getBusinessPics');

    Route::get('/getLastAnouncementJson','MobileControllerV1@getLastAnouncementJson');
    Route::controller('/anouncements', 'AnouncementsController');
    //Route::get('/anouncements/getPicDelete/{id}','AnouncementsController@getPicDelete');

    Route::controller('/notifications', 'NotificationsController');


});
