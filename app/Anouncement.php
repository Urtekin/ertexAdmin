<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anouncement extends Model
{

    public function pictures()
    {
      //return $this->hasMany('App\Comment', 'foreign_key');
    	return $this->hasMany('App\Pictures','anouncement_id')->orderBy('id','asc');
    }

}
