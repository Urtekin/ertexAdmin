@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Hoşgeldiniz.</div>
                <div class="panel-body">
                    Ertex Mobil Applikasyon Yönetim Sayfasına Hoşgeldiniz.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
