@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">{{ ucfirst('Duyurular') }}</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        {{ ucfirst('Duyurular') }} Listesi
    </div>

    <div class="panel-body">
        <div class="">
            <table class="table table-striped" id="thegrid">
              <thead>
                <tr>
                                        <th>Id</th>
                                        <th>Başlık</th>
                                        <th>İçerik</th>
                                        <th>Aktif</th>
                                        <th>Başlangıç Tarihi</th>
                                        <th>Güncelleme Tarihi</th>
                                        <th style="width:50px"></th>
                    <th style="width:50px"></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
        <a href="{{url('anouncements/add')}}" class="btn btn-primary" role="button">Duyuru Ekle</a>
    </div>
</div>




@endsection



@section('scripts')
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            theGrid = $('#thegrid').DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "responsive": true,
                "ajax": "{{url('anouncements/grid')}}",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{url('anouncements/show')}}/'+row[0]+'">'+data +'</a>';
                        },
                        "targets": 1
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{url('anouncements/update')}}/'+row[0]+'" class="btn btn-default">Güncelle</a>';
                        },
                        "targets": 6                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{url('anouncements/delete')}}" onclick="return doDelete('+row[0]+')" class="btn btn-danger">Sil</a>';
                        },
                        "targets": 6+1
                    },
                ]
            });
        });
        function doDelete(id) {
            if(confirm('Duyuruyu silmek istediğinizden emin misiniz?')) {
               $.ajax('{{url('anouncements/delete')}}/'+id).success(function() {
                theGrid.ajax.reload();
               });

            }
            return false;
        }
    </script>
@endsection
