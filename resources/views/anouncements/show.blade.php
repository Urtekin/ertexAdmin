@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Duyurular</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Duyuruları Gör    </div>

    <div class="panel-body">
                
        <form action="{{ url('/anouncements') }}" method="POST" class="form-horizontal">
                
        <div class="form-group">
            <label for="id" class="col-sm-3 control-label">Id</label>
            <div class="col-sm-6">
                <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="title" class="col-sm-3 control-label">Başlık</label>
            <div class="col-sm-6">
                <input type="text" name="title" id="title" class="form-control" value="{{$model['title'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="content" class="col-sm-3 control-label">İçerik</label>
            <div class="col-sm-6">
                <input type="text" name="content" id="content" class="form-control" value="{{$model['content'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="isActive" class="col-sm-3 control-label">Aktif</label>
            <div class="col-sm-6">
                <input type="text" name="isActive" id="isActive" class="form-control" value="{{$model['isActive'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_at" class="col-sm-3 control-label">Başlangıç Tarihi</label>
            <div class="col-sm-6">
                <input type="text" name="created_at" id="created_at" class="form-control" value="{{$model['created_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="updated_at" class="col-sm-3 control-label">Güncelleme Tarihi</label>
            <div class="col-sm-6">
                <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{$model['updated_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/anouncements') }}"><i class="glyphicon glyphicon-chevron-left"></i> Geri</a>
            </div>
        </div>

        </form>
    @if(isset($picModel ))
         <form action="{{ url('/anouncements/save') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }}
        @foreach($picModel as $picture) 
        
        <img src="/uploads/{{$picture['name']}}"  id="{{$picture['id']}}" style="width:304px;height:228px; margin:10px;">
        </img>
        
        @endforeach
        </form>
        @endif

    </div>
</div>







@endsection