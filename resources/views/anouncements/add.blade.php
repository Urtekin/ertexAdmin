@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">Duyurular</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Duyuru Ekle/Düzenle
    </div>
    <link rel="stylesheet" href="/css/add.css">
    <div class="panel-body">

        <form action="{{ url('/anouncements/save') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }}
               <div class="form-group">
                <div class="col-sm-6">
                    <input type="text" name="title" id="title" class="form-control" style="visibility:hidden;"value="{{$model['id'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="title" class="col-sm-3 control-label">Başlık</label>
                <div class="col-sm-6">
                    <input type="text" name="title" id="title" class="form-control" value="{{$model['title'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="content" class="col-sm-3 control-label">İçerik</label>
                <div class="col-sm-6">
                    <input type="text" name="content" id="content" class="form-control" value="{{$model['content'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="isActive" class="col-sm-3 control-label">Notification</label>
                <div class="col-sm-2">
                    <input name="isActive" id="isActive" class="form-control" type="checkbox">
                </div>
            </div>
             <div class="form-group">
                <label for="pictures" class="col-sm-3 control-label">Resimler</label>
                <div class="col-sm-6">
                   <div id="filediv"><input name="file[]" type="file" id="file"/></div>
                   <input type="button" id="add_more" class="upload" value="Add More Files"/>
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Kaydet
                    </button>
                    <a class="btn btn-default" href="{{ url('/anouncements') }}"><i class="glyphicon glyphicon-chevron-left"></i> Geri</a>
                </div>
            </div>
        </form>
    </br>
    @if(isset($picModel ))
         <form action="{{ url('/anouncements/save') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }}
        @foreach($picModel as $picture)

        <img src="/uploads/{{$picture['name']}}"  id="{{$picture['id']}}" style="width:304px;height:228px; margin:10px;">
            <img id='{{ $picture['id'] }}' value="picDelete" class="img2" src='/pictures/x.png' alt='delete'/>
        </img>

        @endforeach
        </form>
        @endif
    </div>
</div>
    <script src="/js/add.js"></script>
@endsection
