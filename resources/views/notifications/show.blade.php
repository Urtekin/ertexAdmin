@extends('crudgenerator::layouts.master')

@section('content')



<h2 class="page-header">Mesaj</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Mesaj    </div>

    <div class="panel-body">
                
        <form action="{{ url('/notifications') }}" method="POST" class="form-horizontal">
                
        
                
        <div class="form-group">
            <label for="title" class="col-sm-3 control-label">Başlık</label>
            <div class="col-sm-6">
                <input type="text" name="title" id="title" class="form-control" value="{{$model['title'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="content" class="col-sm-3 control-label">İçerik</label>
            <div class="col-sm-6">
                <input type="text" name="content" id="content" class="form-control" value="{{$model['content'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_at" class="col-sm-3 control-label">Gönderilme Zamanı</label>
            <div class="col-sm-6">
                <input type="text" name="created_at" id="created_at" class="form-control" value="{{$model['created_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/notifications') }}"><i class="glyphicon glyphicon-chevron-left"></i> Geri</a>
            </div>
        </div>

        </form>
    

    </div>
</div>







@endsection