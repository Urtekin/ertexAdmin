@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">Mesaj</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Kullanıcılara Mesaj Gönder    </div>

    <div class="panel-body">
                
        <form action="{{ url('/notifications/save') }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title" class="col-sm-3 control-label">Başlık</label>
                <div class="col-sm-6">
                    <input type="text" name="title" id="title" class="form-control" value="{{$model['title'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="content" class="col-sm-3 control-label">İçerik</label>
                <div class="col-sm-6">
                    <input type="text" name="content" id="content" class="form-control" value="{{$model['content'] or ''}}">
                </div>
            </div>            
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Gönder
                    </button> 
                    <a class="btn btn-default" href="{{ url('/notifications') }}"><i class="glyphicon glyphicon-chevron-left"></i> Geri</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection