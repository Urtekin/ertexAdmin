@extends('crudgenerator::layouts.master')

@section('content')


<h2 class="page-header">{{ ucfirst('notifications') }}</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        List of {{ ucfirst('notifications') }}
    </div>

    <div class="panel-body">
        <div class="">
            <table class="table table-striped" id="thegrid">
              <thead>
                <tr>
                                        <th>Mesaj sırası</th>
                                        <th>Title</th>
                                        <th>Content</th>
                                        <th style="width:50px"></th>
                    <th style="width:50px"></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
        <a href="{{url('notifications/add')}}" class="btn btn-primary" role="button">Mesaj Ekle</a>
    </div>
</div>




@endsection



@section('scripts')
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            theGrid = $('#thegrid').DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "responsive": true,
                "ajax": "{{url('notifications/grid')}}",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{url('notifications/show')}}/'+row[0]+'">'+data +'</a>';
                        },
                        "targets": 1
                    }
                ]
            });
        });
        function doDelete(id) {
            if(confirm('You really want to delete this record?')) {
               $.ajax('{{url('notifications/delete')}}/'+id).success(function() {
                theGrid.ajax.reload();
               });
                
            }
            return false;
        }
    </script>
@endsection